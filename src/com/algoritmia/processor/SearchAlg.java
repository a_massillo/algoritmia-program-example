package com.algoritmia.processor;

import java.util.ArrayList;
import java.util.List;


import com.algoritmia.tree.Node;

/**
 * 
 * @author amassillo
 *
 */
public class SearchAlg {

	/**
	 * 
	 * @param pRoot
	 * @return
	 */
	public void search(Node pRoot, List<List<String>> pResult) {
		//if leaf, start new list, ordered among other lists if any
		if (pRoot.isLeaf()) {
			ArrayList<String> lNewList = new ArrayList<String>();
			lNewList.add(pRoot.getValue());
			//starts new list
			if (pResult.size() < 1)
				pResult.add(lNewList);
			else {
				int lIndex = 0;
				for (List<String> lResult : pResult) {
					if (lResult.size() < 1 || lResult.get(0).compareTo(pRoot.getValue()) > 1) {
						pResult.add(lIndex,lNewList);
						break;
					}
					lIndex++;
				}
				//not found, add at the end
				if (lIndex == pResult.size())
					pResult.add(lNewList);
			}
		}else{
			for(Node lChildNode : pRoot.getChildren()) {
				search(lChildNode,pResult);
				//add parent to existent children list
				for (List<String> lResult : pResult) {
					if (lResult.contains(lChildNode.getValue()))
						lResult.add(pRoot.getValue());
				}
			}
		}
	}
	

}
