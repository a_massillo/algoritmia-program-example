package com.algoritmia.processor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.algoritmia.input.TreeFromFile;
import com.algoritmia.tree.Node;

/**
 * 
 * @author amassillo
 *
 */

public class Main {

	public static void main(String[] args) {
		if (args == null || args.length < 0) {
			System.out.println("Please provide a valid file name");
			return;
		}
		Node root;
		try {
			root = new TreeFromFile().load(args[0]);
		}catch(FileNotFoundException ex) {
			System.out.println("Unable to find file");
			return;
		}catch(IOException ex) {
			System.out.println("Unable to process file");
			return;
		}catch(Exception ex) {
			System.out.println("An unexpected error has ocurred: " + ex.getMessage());
			return;
		}
		List<List<String>> result = new ArrayList<List<String>>();
		/*//tree sample
		Node root2 = new Node("Genero",new Node("Rock", 
												new Node("Nacional", new Node("Viejito"), new Node("Moderno")), 
												new Node("Extranjero")),
									   new Node("Jazz"));
		Node root3 = new Node("A", new Node("X"), 
										new Node("B", new Node("C")), 
										new Node("D", new Node("E"), new Node("F", new Node("G"))));
		*/
		new SearchAlg().search(root, result);
		//print result
		for (List<String> lResult: result) {
			for (String lsubResult: lResult) 
				System.out.print(lsubResult + " ");
			System.out.println();
		}
	}
}
