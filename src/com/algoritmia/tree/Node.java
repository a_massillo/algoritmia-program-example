package com.algoritmia.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author amassillo
 *
 */
public class Node {

	private String value;
	private List<Node> children;

	public Node(String value, Node...pNode2s) {
		this.value = value;
		this.children = new ArrayList<Node>();
		if (pNode2s !=null)
			for(Node lNode: pNode2s)
				this.addChildren(lNode);
	}
	
	public void addChildren (Node pNode) {
		this.children.add(pNode);
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public List<Node> getChildren() {
		return children;
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public boolean isLeaf() {
		return this.getChildren() == null || this.getChildren().isEmpty();
	}
	
}
