package com.algoritmia.input;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.algoritmia.tree.Node;

/**
 * 
 * @author amassillo
 *
 */
public class TreeFromFile {
	
	Node rootNode = null;
	
	/**
	 * 
	 * @param pFileName
	 * @return
	 */
	public Node load(String pFileName) throws FileNotFoundException, IOException{
		
		BufferedReader br = new BufferedReader(new FileReader(pFileName));
		String line;
		while ((line = br.readLine()) != null) {
			 // process the line.
			String[] lNodes = line.split(" ");
			String lParentNode = lNodes[0];
			String lChildNode = lNodes[1]; //node to insert
			if (rootNode == null)
				rootNode = new Node(lParentNode, new Node(lChildNode));
			else {
				Node lNode2 = this.searchParent(rootNode, lParentNode);
				if (lNode2 != null)
					lNode2.getChildren().add(new Node (lChildNode));
				else
					rootNode.addChildren(new Node (lChildNode));
			}	
		}
		br.close();
		return rootNode; 
	}
	
	/**
	 * 
	 * @param pStartNode
	 * @param pValue
	 * @return
	 */
	public Node searchParent(Node pStartNode, String pValue) {
		if (pStartNode != null) {
			if (pStartNode.getValue().equals(pValue)) 
				return pStartNode;
			for (Node lNode : pStartNode.getChildren()) {
				Node lNodePartial = this.searchParent(lNode, pValue);
				if (lNodePartial !=null)
					return lNodePartial;
			}
		}
		return null;
	}
	
}
